//GULP
var gulp = require("gulp");

//Plugins
var jshint = require("gulp-jshint");
var sass = require("gulp-sass");
var concat = require("gulp-concat");
var uglify = require("gulp-uglify");
var rename = require("gulp-rename");
var browserSync = require('browser-sync').create();
var minifyCss = require("gulp-clean-css");

//Lint
gulp.task("lint", function() {
    return gulp.src("js/*.js")
    .pipe(jshint())
    .pipe(jshint.reporter("default"));
});

//SASS
gulp.task("sass", function () {
    return gulp.src("scss/*.scss")
    .pipe(sass())
    .pipe(concat("styles.css"))
    .pipe(gulp.dest("dist/css"))
    .pipe(minifyCss())
    .pipe(rename("styles.min.css"))
    .pipe(gulp.dest("dist/css"))
    ;
});

//Scripts
gulp.task("scripts", function() {
    return gulp.src("js/*.js")
    .pipe(concat("scripts.js"))
    .pipe(gulp.dest("dist/js"))
    .pipe(rename("scripts.min.js"))
    .pipe(uglify())
    .pipe(gulp.dest("dist/js"));
});

// Serve
gulp.task("serve", ["sass", "scripts", "lint"], function() {
    browserSync.init({
        server: "./"
    });

    gulp.watch("scss/*.scss", ["sass", browserSync.reload]);
    gulp.watch("**/*.html").on("change", browserSync.reload);
    gulp.watch("js/*.js", ["scripts", "lint", browserSync.reload]);
});


//Default

gulp.task("default", ["serve"]);
gulp.task("build", ["sass", "scripts", "lint"]);